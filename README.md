# README #
![Captura.png](https://bitbucket.org/repo/yyzXeX/images/239859884-Captura.png)

## Versión 1.0 ##

Raquetas se mueven en 2D, si no se presionan las teclas se paran.
Las raquetas tienen sus propios límites.
La esfera se va reduciendo con el tiempo hasta llegar a un mínimo, en caso de reiniciarse la esfera, esta recupera su valor original.


jugador 1  teclas: **w**,**s**,**a**,**d**  
jugador 2  teclas: **arr**,**ab**,**izq**,**dcha**

*Sirven también las mayúsculas*

*Pendiente: Comencé las clase ListaEsferas todavía no funciona adecuadamente*

## Versión 2.0 ##
Creación del logger con la particularidad que este pasa sólo un vector de 2 elementos y el logger va guardando en memoria el resultado.

Logger más limpio con  refresco y borrado de pantalla