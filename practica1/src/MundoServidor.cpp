// Mundo.cpp: implementation of the CMundoServidor
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
///////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <error.h>
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void* hilo_comandos(void* d); 
void* hilo_conexiones(void* c)
{
	CMundoServidor* p=(CMundoServidor*) c;
	p->GestionaConexiones();
 }
char ip[] = "127.0.0.1";
//char ip[] = "192.168.1.73";

CMundoServidor::CMundoServidor():puerto(3550)
{	
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd);
	unlink("/tmp/logger");
	//munmap(MemCp,sizeof(MemC));
	//close(tuberia_servidor_cliente);
	//close(tuberia_cliente_servidor_teclas);
	//close(tuberia_cliente_servidor_teclas_esp);
    conectar.Close(); 
    comunicar.Close();
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",datos1[1]);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",datos2[1]);
	print(cad,650,0,1,1,1);
	sprintf(cad,"SERVIDOR  SOLO ESPECTADOR");
	print(cad,320,10,21,30,41);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	//esfera2.Dibuja();
	esferas.Dibuja();
	esferas.Inicializa();


	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Reduccion(1);
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		datos2[1]++;
		if(fd >= 0) write(fd, datos2, sizeof(datos2));
		esfera.radio=0.5;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;

		
		esfera2.centro.x=0;
		esfera2.centro.y=rand()/(float)RAND_MAX;
		esfera2.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera2.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		datos1[1]++;
		if(fd >= 0) write(fd, datos1, sizeof(datos1));
		esfera.radio=0.5;
	}
/*	if(MemCp->accion== 1) OnKeyboardDown('w',1,1);
	if(MemCp->accion==-1) OnKeyboardDown('s',1,1);
	MemCp->esfera=esfera;
	MemCp->raqueta1=jugador1;
*/
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, datos1[1], datos2[1]);
	//write(tuberia_servidor_cliente, cad, sizeof(cad));
    comunicar.Send(cad, sizeof(cad)); 
    /*if (datos1[1]>=3||datos2[1]>=3)
    {
    	std::cout<<"Partida terminada"<<std::endl;
    	exit(0);
    }*/
	//PRACTICA 5
	for (int i=conexiones.size()-1; i>=0; i--)
	 {
         	if (conexiones[i].Send(cad,200) <= 0)
		 {
            		conexiones.erase(conexiones.begin()+i);
           	 	if (i < 2) // Hay menos de dos clientes conectados
			{
				datos1[1]=0;
				datos2[1]=0;
				/*if (conexiones[0].Send(cad,200) <= 0)
				{	
					conexiones[0]=conexiones[2];
					for(i=conexiones.size()-1;i>=2;i--)
					{
						conexiones[i-1]=conexiones[i];
					}
				}
				if (conexiones[1].Send(cad,200) <= 0)
				{
					for(i=conexiones.size()-1;i>=1;i--)
					{
						conexiones[i-1]=conexiones[i];
					}
				}
             			 // Se resetean los puntos a cero*/
        	 	}
     		}
	}

}

void CMundoServidor::Init()
{
	Tuberia();
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

//Fondo izq 
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;
//Fondo dcho
	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Creacion del thread que sirve como hilo entre el servidor y el cliente
	pthread_create(&th1, NULL, hilo_comandos, this);
	char cad[100]; 
	
	conectar.Connect(ip, puerto);
    conectar.InitServer(ip, puerto);

	comunicar = conectar.Accept(); 
    comunicar.Receive(cad, strlen(cad)+1);
	std::cout << "Nombre del jugador: "<<cad << std::endl;
	pthread_create(&conexiones,NULL, hilo_conexiones, this);
}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            unsigned char key;

            comunicar.Receive(cad, sizeof(cad));
           // read(tuberia_cliente_servidor_teclas, cad, sizeof(cad));
            sscanf(cad,"%c",&key);
            switch(key)
			{
    			case 'w':case 'W':jugador1.velocidad.y=+1;break;
    			case 's':case 'S':jugador1.velocidad.y=-1;break;
    			case 'i':case 'I':jugador2.velocidad.y=+1;break;
    			case 'k':case 'K':jugador2.velocidad.y=-1;break;
			}
     }
}
void CMundoServidor::Tuberia()
{
	datos1[0]=1;	datos1[1]=0;
	datos2[0]=2;	datos2[1]=0;

	fd = open("/tmp/logger", O_WRONLY);
	if(fd < 0)
	  printf("WARNING al abrir la tubería\n1º Cierra el programa\n2º Ejecuta el logger\n3º Ejecuta tenis de nuevo\n");
	//hay que finalizar los printf con \n

	/*tuberia_servidor_cliente = open ("/tmp/tuberia_servidor_cliente", O_WRONLY); 
	tuberia_cliente_servidor_teclas = open ("/tmp/tuberia_cliente_servidor_teclas",O_RDONLY);
	tuberia_cliente_servidor_teclas_esp = open ("/tmp/tuberia_cliente_servidor_teclas_esp",O_RDONLY);*/

}
void CMundoServidor::GestionaConexiones(){
	while(1)
	{
		conexiones=conectar.Accept();
	}
}