INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")
FIND_PACKAGE (Threads)

SET(COMMON_SRCS 
	Esfera.cpp
	ListaEsferas.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)

ADD_EXECUTABLE(servidor servidor.cpp ${COMMON_SRCS}	MundoServidor.cpp )
ADD_EXECUTABLE(cliente cliente.cpp ${COMMON_SRCS}  MundoCliente.cpp )
ADD_EXECUTABLE(logger logger.cpp )
ADD_EXECUTABLE(bot bot.cpp )

TARGET_LINK_LIBRARIES(servidor glut GL GLU)
TARGET_LINK_LIBRARIES(cliente glut GL GLU)
TARGET_LINK_LIBRARIES(bot glut GL GLU)
TARGET_LINK_LIBRARIES(servidor ${CMAKE_THREAD_LIBS_INIT})