// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	velocidad.y=0;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	//Restricciones de espacio
	if(x1<=-6)x1=-6;
	if(x2>=6) x1=6;

	x1=x2=x1+0.1*velocidad.x;		
	y1=y1+0.1*velocidad.y;
	y2=y2+0.1*velocidad.y;
}
