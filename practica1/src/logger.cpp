#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>		//para borrar pantalla
#include <stdio.h>


int main(){
  
  int crear_ok;
  int datos[2];			   //Quién es el jugador que marca y su puntuación	
  int result[2]={0,0};		//Guarda los resultados que le van llegando
  
  printf("Esperando a que ejecutes ./servidor\n");
  mkfifo("/tmp/logger", 0666 );
  int abierta_ok = open("/tmp/logger", O_RDONLY);
  if(abierta_ok < 0){ //Devuelve un descriptor de archivo o -1 en caso de error
    printf("ERROR al abrir FIFO.\n"); 
    return 0;
  }
 while(1){
    int fd=read(abierta_ok, datos, sizeof(datos));
    if(fd <= 0){
      printf("Error al abrir el \n");
      break;//cuando lo lee termina todo return sale de todo el logger
    }else  if(fd < 0){

    }
    system("clear");		//Borra la pantalla
    if(datos[0]==1)	result[0]=datos[1];
    else			      result[1]=datos[1];
    printf("JUGADOR 1 =  %d puntos\nJUGADOR 2 =  %d puntos\n",result[0],result[1]);

  }
  close(abierta_ok);
  unlink("/tmp/logger");
  return 0;
}