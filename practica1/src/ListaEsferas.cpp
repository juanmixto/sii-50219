#include "ListaEsferas.h"
ListaEsferas::ListaEsferas(void){
	numero=0;
	for(int i=0;i<MAX_ESFERAS;i++)
	lista[i]=0;
}
bool ListaEsferas::Agregar (Esfera *e)
{
	if(numero<MAX_ESFERAS)
		lista[numero++]=e;
	else
		return false;
	return true;
}
void ListaEsferas::Inicializa(){
	for (int i=0;i<numero;i++) {
		lista[i]->centro.x=1;
		lista[i]->centro.y=1;
	}
}

void ListaEsferas::Dibuja(){
	for (int i=0;i<numero;i++) lista[i]->Dibuja();

}