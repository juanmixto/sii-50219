#include <vector>
#include "Plano.h"

#include "ListaEsferas.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"
#include <iostream>

class CMundoCliente  
{
public:

	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnKeyboardUp	(unsigned char key, int x, int y);
	void TeclaEspecial(unsigned char key);
	void TeclaEspecialSoltar(unsigned char key);
	void OnTimer(int value);
	void OnDraw();	

	void MemoriaCompartida();

	Esfera esfera, esfera2;
	ListaEsferas esferas;
	std::vector<Plano> paredes;
	Plano fondo_izq,fondo_dcho;
	Raqueta jugador1, jugador2;

	int datos1[2],datos2[2]; // [0]-> Número del jugador [1]-> Puntos del jugador
	DatosMemCompartida MemC,*MemCp;
 	int fd,fd3;
 //	int tuberia_servidor_cliente;
 //	int tuberia_cliente_servidor_teclas;
 //	int tuberia_cliente_servidor_teclas_esp;
 	Socket comunicar;
};

