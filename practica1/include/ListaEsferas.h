#include "Esfera.h"
#define MAX_ESFERAS 100
class ListaEsferas
{
public:
	ListaEsferas(void);
	void Inicializa();
	void Dibuja();
	bool Agregar (Esfera *e);
private:
	Esfera * lista[MAX_ESFERAS];
	int numero;

};