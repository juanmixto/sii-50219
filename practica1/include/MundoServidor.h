// Mundo.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

//#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
//#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
/*
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
*/
#include "ListaEsferas.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include <pthread.h>
#include "Socket.h"
#include <iostream>


class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	void Tuberia();

	Esfera esfera, esfera2;
	ListaEsferas esferas;
	std::vector<Plano> paredes;
	Plano fondo_izq,fondo_dcho;
	Raqueta jugador1, jugador2;

	int datos1[2],datos2[2]; // [0]-> Número del jugador [1]-> Puntos del jugador
	DatosMemCompartida MemC,*MemCp;
 	int fd;
 	//int tuberia_servidor_cliente; 
 	//int tuberia_cliente_servidor_teclas;
 	//int tuberia_cliente_servidor_teclas_esp;
 	Socket conectar,comunicar;
 	pthread_t th1; 
 	int puerto;
 	//char ip[11] = "127.0.0.1";
};

